class RequestController < ApplicationController
	
	def home
		render :action => "index"
	end

	def penticon
		@arg = params[:name]
		@image_svg_data = Penticon.uri_image(@arg)
		render :action => "penticon_final"
	end
end
