Rails.application.routes.draw do
  root 'request#home'
  post '/penticon' => 'request#penticon'
end
